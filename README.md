# Weapon Control Code 

This code controlls the AVR microcontroller embedded in the weapon.

The exact microcontroller used here is the MicroChip Attiny 861A.
A 261/461 would be fine as well. They have 2K/4K Flash and 128/256 Bytes of SRAM respectively.

[Datasheet](http://ww1.microchip.com/downloads/en/DeviceDoc/doc8197.pdf)

## AVR Configuration
The microcontroller has to run with full 8 MHz without clock divider. It is also recommend to enable brown-out detection.  
Recommended fuse settings:  
``` -U lfuse:w:0xe2:m -U hfuse:w:0xdd:m -U efuse:w:0xff:m  ```

If you use a different device or frequency you will have to adjust the CMakeList.txt:
```
SET(DEVICE "attiny861")
SET(FREQ "8000000")
add_definitions(-DATTINY861)
```

## Function
This handles three main tasks
1. Transmit IR signals (shoot)
2. Control and display (LED) the weapon status (ammunition etc.)
3. Receive configuration from player master (ESP) via I2C

### Pins
- 18 (SCL): I2C clock
- 20 (SDA): I2C data
- 8 (PB5): IR Control (PWM)
- 4 (PB3): Speaker output (PWM)
- 7 (PB4): Flash LED
- 9 (PB6): Trigger Pin (Input)
- 11 (PA7): Ammunition LED
- 12 (PA6): Magazine LED
- 13 (PA5): Kill confirm LED  
- 14 (PA4): Reload Pin (Input)


###### Speaker
Very basic shooting sound

###### Ammunition in Magazine LED
Signal number of bullets in magazines:
- Off: Some ammo
- On: Out of ammo

###### Magazine Count LED
Signal number of remaining magazines:
- Off: Magazine full
- Blinking: One magazine left
- On: No more magazines left

###### Reload Trigger Pin
Low active - Pullup
Trigger reload

###### Kill confirm LED
Blink on confirmed kill (received from ESP via I2C)

###### I2C
Slave
For specification check: https://gitlab.com/TeamChaos/LaserTag/Concept/blob/master/Network/Protocols/WeaponControl.md

## Code
### Program structure
### Sound Playback
The used sounds are generated frequency sweeps/chirps. One timer is used in CTC mode to generate a counter match interrupt with the desired sample frequency (e.g. 24.000).  
Within the interrupt routine the next sample value is calculated.  
The 8 bit sample value is converted to an "analog" output using the second timer. It is running in FastPWM mode and constantly counts up.  
Once the timer matches the output is set to LOW, once the counter reaches the top limit the output is set to HIGH again and the timer resets to 0.

Since the playback requires two timers there is none available for the normal delay function. Therefore the execution is paused during playback.

### LED Blinking
The LED Blinking is handled in a dedicated method which is called every loop execution (around 40ms if nothing happens).  
It makes the LEDs blink according to the respective desired frequency specified in a global period variable.  
By setting the respective timer to something larger than zero and updating the period variable a is set to blink.  
To stop blinking the respective timer has to be set to 0 and the LED has to be turned off (if it was on).  
  
This concept is rather confusing and stops execution during shooting or sound playback, but since there are only two timers available and we don't want to make the code to complicated we accept this.  

## Development Setup
To work on this project you need the several packages:  
avr-binutils, avr-gcc, and avr-libc  
Don't forget about git of course.  

A CMake compatible IDE is recommended.  
E.g. CLion or KDevelop (free)  
QtCreator is supposed to work as well

