#define F_CPU  8000000UL
//#define DEBUG
//#define TYPE0
#define TYPE1

#include <stdbool.h>
#include <version.h> //Generated git version file
#include "CommonLib/avr_libs/timers.h"
#include "CommonLib/ir/send.h"
#include "CommonLib/avr_libs/TWI_slave.h"
#include "CommonLib/weapon_control/protocol.h"



//LED Magazine status
#define PINID_LED_MAGAZINE PA6
#define PORT_LED_MAGAZINE PORTA
#define DDR_LED_MAGAZINE DDRA
//LED Ammo status
#define PINID_LED_AMMO PA7
#define PORT_LED_AMMO PORTA
#define DDR_LED_AMMO DDRA
//LED Kill confirm
#define PINID_LED_KILL PA5
#define PORT_LED_KILL PORTA
#define DDR_LED_KILL DDRA
//Button reload
#define PINID_BUTTON_RELOAD PA4
#define PORT_BUTTON_RELOAD PORTA
#define DDR_BUTTON_RELOAD DDRA
#define PIN_BUTTON_RELOAD PINA
//Button trigger
#define PINID_BUTTON_TRIGGER PB6
#define PORT_BUTTON_TRIGGER PORTB
#define DDR_BUTTON_TRIGGER DDRB
#define PIN_BUTTON_TRIGGER PINB
//LED Flash
#define PINID_LED_FLASH PB4
#define PORT_LED_FLASH PORTB
#define DDR_LED_FLASH DDRB
//Vibration
#define PINID_VIBRATOR PA3
#define PORT_VIBRATOR PORTA
#define DDR_VIBRATOR DDRA

#define LED_PERIOD_MAG 16

const char gitversion[8] = GIT_COMMIT_HASH;

//Weapon configuration
uint8_t id;
uint8_t damage;
uint8_t fire_delay;
uint8_t fire_mode;
uint8_t magsize;
uint8_t reload_time;

//Current weapon values
volatile uint8_t status; //Holds information if the respective weapon configuration have been set. If this is not 0xFF the weapon is not ready to fire.
volatile uint8_t enabled;
uint8_t ammunition;
volatile uint8_t mags;
uint16_t shots_fired;




//Predeclare functions
void inline shoot();

void inline reload();
uint8_t onI2CRequest(uint8_t addr);
void onI2CReceive(uint8_t addr, uint8_t data);




/*
 * Should be called after the weapon configuration has been finished.
 * Updates LEDs /Ammo etc.
 */
void finishConfiguration();

/*
 * Disables the weapon and resets any local variables
 */
void resetInit();

/**
 * Enable the weapon (updates LEDs)
 */
void enable();

/**
 * Disables the weapon (disables LEDs)
 */
void disable();

/**
 * Update shot cooldown and trigger release.
 * Inlined for speed
 * @param delayed Time in ms since the last call to this function. Must not be higher than 200
 */
void updateFireStatus(uint8_t delayed);

/**
 * Optimised for speed, also inlined
 * @return  if the weapon should shoot (cooldown, ready, ammunition, trigger)
 */
bool inline checkShouldShoot();

/**
 * Does play an appropriate sound (if trigger is pressed, but we cannot shoot).
 * Inline because only used once
 * @return  if the weapon should shoot (cooldown, ready, ammunition, trigger)
 */
bool inline checkShouldShootSound();


void inline initiateCooldown();

/**
 * Update LEDs
 */
void updateLEDStatus();


//Fire status/cooldown variables
uint8_t fs_waitForRelease = 0; //If the trigger has to be released before next shot (wait until 0)
uint8_t fs_cooldown = 0; //Cooldown until the next shot can be fired. 1 = 40ms
uint8_t fs_cooldown_sub = 0; //Sub cooldown counter. 1 = 1ms



//Blink variables
uint8_t led_timer_kill;
uint8_t led_timer_mag;

//Force a silent reload upon weapon configuration
volatile bool forceReload=false;



//Sound playback variables
volatile bool playback_active; //If the playback us currently active which means timers are inactive. Can be used in I2C interrupts
uint8_t sound_amp;
uint8_t sound_count1;
uint8_t sound_count1_max;
uint8_t sound_count2;
uint8_t sound_count2_max;
uint8_t sound_ath; //Alternate threshold
uint8_t sound_ac; //Alternate counter
uint8_t sound_ath_limit; //Limit alternate threshold -> Stop sound
bool sound_ath_inc; //Increase or Decrease alternate threshold
bool sound_neg;
bool allow_sound_interrupt = false;
uint8_t sound_fs_check_count;

enum SOUND {
    SHOT,
    LAST_SHOT,
    RELOAD,
    EMPTY,
    FAIL
};

/**
 *
 * @param sound
 * @param duration Sound duration. Only used for reload. 1 ~= 5ms
 */
void playSound(enum SOUND sound, uint16_t duration) {
    cli();
    pauseTimers();
    sound_ac = 0;
    sound_count1 = 0;
    sound_count2 = 0;
    sound_fs_check_count = 0;
    sound_neg = 0;
    switch (sound) {
        case SHOT:
            sound_amp = 127;
            sound_count1_max = 0b01000000;
            sound_count2_max = 0;
            sound_ath = 1;
            sound_ath_limit = 180;
            sound_ath_inc = true;
            break;
        case LAST_SHOT:
            sound_amp = 127;
            sound_count1_max = 0b01110000;
            sound_count2_max = 0;
            sound_ath = 20;
            sound_ath_limit = 180;
            sound_ath_inc = true;
            break;
        case RELOAD:
            sound_amp = 127;
            sound_count1_max = (duration & 0xFF);
            sound_count2_max = (duration >> 8);
            sound_ath = 200;
            sound_ath_limit = 0;
            sound_ath_inc = false;
            break;
        case EMPTY:
            sound_amp = 127;
            sound_count1_max = 0b1000000;
            sound_count2_max = 0b10;
            sound_ath = 170;
            sound_ath_limit = 180;
            sound_ath_inc = true;
            break;
        case FAIL:
            sound_amp = 127;
            sound_count1_max = 0b11111000;
            sound_count2_max = 0;
            sound_ath = 245;
            sound_ath_limit = 255;
            sound_ath_inc = true;
            break;
    }
    SOUND_OUT_PWM_REG = 128;
    SETUP_SOUND_OUT(48000);
    playback_active = true;
    sei();
    delay(1);//Basically waits until sound is finished as timers are paused during playback
}



/*Interrupt routine for sound playback (at sample rate)
  Calculates sample values. Creates a sweep like sequence.
 */
ISR(TIMER0_COMPA_vect) {
    if (sound_ac >= sound_ath) {
        sound_ac = 0;
        sound_neg = !sound_neg;
    }

    sound_count1++;
    SOUND_OUT_PWM_REG = sound_neg ? 127U - sound_amp : 128U + sound_amp;
    sound_ac++;

    sound_fs_check_count++;
    if (sound_fs_check_count == 255) {
        sound_fs_check_count = 0;
        updateFireStatus(5);
        if (allow_sound_interrupt) {
            if (checkShouldShoot()) {
                DISABLE_SOUND_OUT();
                initTimers();
                playback_active = false;
            }
        }
    }

    if (sound_count1 >= sound_count1_max && sound_count2 == sound_count2_max) {
        sound_count2 = 0;
        sound_count1 = 0;
        if ((sound_ath & 0b11) == 0) { //Decrease amplitude every 3 ath (frequency) steps
            sound_amp -= 1;
        }
        if (sound_ath_inc) {
            sound_ath += 1;
        } else {
            sound_ath -= 1;
        }
        if (sound_ath_inc ? (sound_ath >= sound_ath_limit) : (sound_ath <= sound_ath_limit)) {
            DISABLE_SOUND_OUT();
            initTimers();
            playback_active = false;
        }
    } else if (sound_count1 == 255) {
        sound_count2++;
        sound_count1 = 0;
    }
}

void disable() {
    led_timer_kill = 0;
    PORT_LED_KILL &= ~(1 << PINID_LED_KILL);
    PORT_LED_MAGAZINE &= ~(1 << PINID_LED_MAGAZINE);
    PORT_LED_AMMO &= ~(1 << PINID_LED_AMMO);
    enabled = false;
}

void enable() {
    enabled = true;
    //Blink kill LED for 1 sec to indicate enabled
    led_timer_kill = 25;
    PORT_LED_KILL |= (1 << PINID_LED_KILL);

}

void resetInit() {
    disable();
    status = 0b10000000;
    ammunition = 0;
    shots_fired = 0;
    fs_waitForRelease = 0;
    mags = 0;
    fs_cooldown = 0;
    if (playback_active) {
        DISABLE_SOUND_OUT();
        initTimers();
        playback_active = false;
    }

}

void finishConfiguration() {
    if (status == 0xFF) {
        forceReload=true;
    }
}

void inline confirmKill(uint8_t time){
    led_timer_kill=time;
    PORT_LED_KILL |= (1 << PINID_LED_KILL);
}

void updateLEDStatus() {
    if(status==0xFF){
        if(enabled){
            if(mags==0){
                PORT_LED_MAGAZINE |= (1 << PINID_LED_MAGAZINE);
            } else if(mags==1){
                led_timer_mag++;
                if(led_timer_mag>LED_PERIOD_MAG){
                    PORT_LED_MAGAZINE &= ~(1 << PINID_LED_MAGAZINE);
                    led_timer_mag=0;
                }
                else if(led_timer_mag>(LED_PERIOD_MAG>>1)){
                    PORT_LED_MAGAZINE |= (1 << PINID_LED_MAGAZINE);
                }
            }
            else{
                PORT_LED_MAGAZINE &= ~(1 << PINID_LED_MAGAZINE);
            }
            if(ammunition==0){
                PORT_LED_AMMO |= (1 << PINID_LED_AMMO);
            }
            else{
                PORT_LED_AMMO &= ~(1 << PINID_LED_AMMO);
            }
            if(led_timer_kill<=0){
                PORT_LED_MAGAZINE &= ~(1 << PINID_LED_KILL);
            }
            else{
                led_timer_kill--;
            }
        }
        else{
            PORT_LED_KILL |= (1 << PINID_LED_KILL);
            PORT_LED_AMMO |= (1 << PINID_LED_AMMO);
            PORT_LED_MAGAZINE |= (1 << PINID_LED_MAGAZINE);
        }
    }
    else {
        PORT_LED_KILL |= (1 << PINID_LED_KILL);
        PORT_LED_AMMO &= ~(1 << PINID_LED_AMMO);
        PORT_LED_MAGAZINE &= ~(1 << PINID_LED_MAGAZINE);
    }
}

void inline updateFireStatus(uint8_t delayed) {
    if (fs_waitForRelease) {
        if (PIN_BUTTON_TRIGGER & (1 << PINID_BUTTON_TRIGGER)) {
            fs_waitForRelease--;
        }
    }
    if (fs_cooldown > 0) {
        fs_cooldown_sub += delayed;//Have to be careful to not get an overflow here
        while (fs_cooldown_sub >= 40 && fs_cooldown > 0) {
            fs_cooldown--;
            fs_cooldown_sub -= 40;
        }
    }
}


bool inline checkShouldShoot() {
    if (fs_cooldown == 0) {
        if (!fs_waitForRelease) {
            return ammunition > 0 && !(PIN_BUTTON_TRIGGER & (1 << PINID_BUTTON_TRIGGER));
        }
    }
    return false;
}


bool checkShouldShootSound() {
    if (!fs_waitForRelease) {
        if (!(PIN_BUTTON_TRIGGER & (1 << PINID_BUTTON_TRIGGER))) {
            if (ammunition > 0) {
                if (fs_cooldown == 0) {
                    return true;
                } else {
                    playSound(FAIL, 0); //Duration: Maybe 100ms
                    updateFireStatus(100);
                }
            } else {
                playSound(EMPTY, 0); //Duration: Maybe 100ms
                updateFireStatus(100);
            }
        }
    }
    return false;
}


void inline initiateCooldown() {
    fs_cooldown = fire_delay;
    fs_cooldown_sub = 0;
    fs_waitForRelease = fire_mode == 0x01 ? 2 :0;
}

void shoot() {

    initiateCooldown();
    if (ammunition != 0xFF) {
        ammunition--;
    }
    shots_fired++;
    PORT_LED_FLASH |= (1 << PINID_LED_FLASH);
    sendIRMessage(true, id, damage); //Around 30 ms
    PORT_VIBRATOR |= (1 << PINID_VIBRATOR);
    PORT_LED_FLASH &= ~(1 << PINID_LED_FLASH);
    delay(31);
    updateFireStatus(60);

    PORT_LED_FLASH |= (1 << PINID_LED_FLASH);
    delay(25);
    PORT_LED_FLASH &= ~(1 << PINID_LED_FLASH);
    delay(17);
    PORT_LED_FLASH |= (1 << PINID_LED_FLASH);
    delay(28);
    PORT_LED_FLASH &= ~(1 << PINID_LED_FLASH);
    delay(1);
    updateFireStatus(70);
    //delay(42);
    //PORT_LED_FLASH |= (1 << PINID_LED_FLASH);
    //delay(20);
    //PORT_LED_FLASH &= ~(1 << PINID_LED_FLASH);
    allow_sound_interrupt = true;
    playSound(ammunition==0?LAST_SHOT:SHOT, 0);
    allow_sound_interrupt = false;
    PORT_VIBRATOR &= ~(1 << PINID_VIBRATOR);

}


void inline reload() {
    if (enabled) {
        if (mags > 0) {
            ammunition = 0;
            //Play sound
            playSound(RELOAD, reload_time * 16);

            ammunition = magsize;
            if (mags != 0xFF) {
                mags--;
            }
            fs_cooldown = 0;

        } else {
            //Play sound
        }
    }
}

void addMags(uint8_t additional_mags) {
    if (mags == 0xFF)return; //We already have infinite ammo, do nothing
    uint16_t new_mags =
            ((uint16_t) mags) + additional_mags; //Avoid overflows (which would result in a reduced number of mags)
    if (new_mags > 0xFE) { //Make sure we do not get infinite ammo
        mags = 0xFE;
    } else {
        mags = (uint8_t) new_mags;
    }
}




void onI2CReceive(uint8_t addr, uint8_t data) {
    switch (addr) {
        case I2C_ENABLE:
            if (data) {
                enable();
            } else {
                disable();
            }
            break;
        case I2C_SETUP1:
            resetInit();
            break;
        case I2C_WEAPON_ID:
            id = data & 0x0F;
            status |= 1;
            break;
        case I2C_DAMAGE:
            damage = data;
            status |= 1 << 1;
            break;
        case I2C_FIRE_DELAY:
            fire_delay = data;
            status |= 1 << 2;
            break;
        case I2C_FIRE_MODE:
            fire_mode = data;
            status |= 1 << 3;
            break;
        case I2C_MAG_SIZE:
            magsize = data;
            status |= 1 << 4;
            break;
        case I2C_INITIAL_MAGS:
            mags = data;
            status |= 1 << 5;
            break;
        case I2C_RELOAD_TIME:
            reload_time = data;
            status |= 1 << 6;
            break;
        case I2C_SETUP2:
            finishConfiguration();
            break;
        case I2C_ADD_MAG:
            addMags(data);
            break;
        case I2C_MAGS:
            mags = data;
            break;
        case I2C_KILL_CONFIRM:
            confirmKill(data);
            break;
        default:
            break;
    }

}

uint8_t onI2CRequest(uint8_t addr) {
    switch (addr) {
        case I2C_ENABLE:
            return enabled;
        case I2C_STATUS:
            return status;
        case I2C_MAGS:
            return mags;
        case I2C_AMMUNITION:
            return ammunition;
        case I2C_SHOTS_FIRED_LSB:
            return shots_fired; //Cast to uint8_t therefore only includes 8 LSB
        case I2C_SHOTS_FIRED_MSB:
            return shots_fired >> 8;
        case I2C_READ_VERSION1:
        case I2C_READ_VERSION2:
        case I2C_READ_VERSION3:
        case I2C_READ_VERSION4:
        case I2C_READ_VERSION5:
        case I2C_READ_VERSION6:
        case I2C_READ_VERSION7:
        case I2C_READ_VERSION8:
            return (uint8_t) gitversion[addr - I2C_READ_VERSION1];
        case I2C_READ_ID:
            return WEAPON_ID;
        default:
            return 0x00;
    }
}





//################### Main Code ###################### //

void loop() {
    if (status == 0xFF && enabled) {
        if(forceReload){
            forceReload=false;
            if(mags>0){
                ammunition=magsize;
                if(mags!=0xFF){
                    mags--;
                }
            }
        }

        bool reloadButton = PIN_BUTTON_RELOAD & (1 << PINID_BUTTON_RELOAD);

#ifdef TYPE0
        reloadButton = !reloadButton;
#endif
        if (reloadButton) {
#if defined(TYPE0)
            reload(); //Duration: Up to several seconds
#elif defined(TYPE1)
            uint8_t counter=0;
            while(counter<5 && (PIN_BUTTON_RELOAD & (1 << PINID_BUTTON_RELOAD))){
                delay(100);
                counter++;
            }
            if(counter>=5){
                reload();
            }
#endif
        } else {
            if (checkShouldShootSound()) {
                shoot();
            }
        }
    }
    delay(40);
    updateFireStatus(40);
    updateLEDStatus();
}

void setup() {
    //Input with pullup
    DDR_BUTTON_TRIGGER &= ~(1 << PINID_BUTTON_TRIGGER);
    PORT_BUTTON_TRIGGER |= (1 << PINID_BUTTON_TRIGGER);
    DDR_BUTTON_RELOAD &= ~(1 << PINID_BUTTON_RELOAD);
    PORT_BUTTON_RELOAD |= (1 << PINID_BUTTON_RELOAD);

    //Output
    DDR_LED_AMMO |= (1 << PINID_LED_AMMO);
    DDR_LED_FLASH |= (1 << PINID_LED_FLASH);
    DDR_LED_KILL |= (1 << PINID_LED_KILL);
    DDR_LED_MAGAZINE |= (1 << PINID_LED_MAGAZINE);
    DDR_VIBRATOR |= (1 << PINID_VIBRATOR);


    i2c_init(WEAPON_I2C_ADDRESS);
    i2c_onRequestPtr = onI2CRequest;
    i2c_onReceivePtr = onI2CReceive;

    resetInit(); //Initialize local variables

#ifdef DEBUG
    //Temporary init values
    id = 0b0001001;
    damage = 0b00000111;
    fire_delay = 5;
    reload_time = 25;
    fire_mode = 0x01;
    magsize = 60;
    mags =3;
    status = 0xFF;
    finishConfiguration();
    enable();

#endif
}


int main(void) {
    initTimers();
    setup();

    //Startup LED flash
    PORT_LED_FLASH |= (1 << PINID_LED_FLASH);  //Set output off
    delay(200);
    PORT_LED_FLASH &= ~(1 << PINID_LED_FLASH);  //Set output off
    delay(100);
    PORT_LED_FLASH |= (1 << PINID_LED_FLASH);  //Set output off
    delay(100);
    PORT_LED_FLASH &= ~(1 << PINID_LED_FLASH);  //Set output off

    PORT_LED_AMMO |= (1 << PINID_LED_AMMO);
    delay(300);
    PORT_LED_AMMO &= ~(1 << PINID_LED_AMMO);
    delay(50);
    PORT_LED_MAGAZINE |= (1 << PINID_LED_MAGAZINE);
    delay(300);
    PORT_LED_MAGAZINE &= ~(1 << PINID_LED_MAGAZINE);
    delay(50);
    PORT_LED_KILL |= (1 << PINID_LED_KILL);
    delay(300);
    PORT_LED_KILL &= ~(1 << PINID_LED_KILL);
    delay(50);


    //END temporary blink

    while (1) {
        loop();
    }
    return 0;
}


